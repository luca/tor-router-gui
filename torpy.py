#!/usr/bin/env python
import sys
import os
import requests
import json
import time
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.uic import loadUiType

ui,_ = loadUiType('src/gui.ui')
class MainApp(QMainWindow , ui):
	def __init__(self , parent=None):
		super(MainApp , self).__init__(parent)
		QMainWindow.__init__(self)
		self.setFixedSize(400, 500)
		self.setupUi(self)
		# Connect Button :
		self.Connect_Button.clicked.connect(self.torconnect)
		self.Connect_Button.clicked.connect(self.net_stat)
		# Disconnect Button :
		self.Disconnect_Button.clicked.connect(self.disconnect)
		self.Disconnect_Button.clicked.connect(self.net_stat)
		# Status :
		self.net_stat()
	
	# Connection Status :	
	def net_stat(self):
		tor_req = requests.get("https://check.torproject.org/api/ip")
		ip = tor_req.json()['IP']
		status = tor_req.json()['IsTor']
		
		# Val :
		if status == True:
			# IP :
			self.ipstat.setStyleSheet("color : lightgreen")
			self.ipstat.setText(ip)
			# Status :
			con = ("Connected")
			self.connstat.setStyleSheet("color : lightgreen")
			self.connstat.setText(con)
			self.img.setStyleSheet("image : url(src/images/tor-on.png);")
			
		elif status == False:
			# IP :
			self.ipstat.setStyleSheet("color : red")
			self.ipstat.setText(ip)
			# Status :
			con = ("Not Connect")
			self.connstat.setStyleSheet("color : red")
			self.connstat.setText(con)
			self.img.setStyleSheet("image : url(src/images/tor-off.png);")

		
	# Connect :
	def torconnect(self):
		start_tor = "pkexec systemctl restart tor && pkexec ./src/script/tor-router"
		os.system(start_tor)
		notif = 'notify-send "Connected Through Tor 🧅" '
		os.system(notif)
		
	# Disconnect :
	def disconnect(self):
		stop_tor = "pkexec systemctl restart iptables && pkexec systemctl restart tor"
		os.system(stop_tor)
		notif = 'notify-send "Disconnect From Tor 🧅" '
		os.system(notif)
		
app = QApplication(sys.argv)
window = MainApp()
window.show()
app.exec_()
